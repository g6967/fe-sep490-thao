import React from "react";
import "./assets/bootstrap.min.css";
import "./assets/style.min.css";
const NavbarSetting = () => {
  return (
    <>
      <div className="col-xl-9">
        <div className="row">
          <div className="col-xl-12">
            <div className="card custom-card">
              <div className="card-body p-0">
                <div className="d-flex p-3 align-items-center justify-content-between">
                  <div>
                    <h6 className="fw-semibold mb-0">Tasks</h6>
                  </div>
                  <div>
                    <ul
                      className="nav nav-tabs nav-tabs-header mb-0 d-sm-flex d-block"
                      role="tablist"
                    >
                      <li className="nav-item m-1">
                        <a
                          className="nav-link active"
                          data-bs-toggle="tab"
                          role="tab"
                          aria-current="page"
                          href="#all-tasks"
                          aria-selected="true"
                        >
                          All Tasks
                        </a>
                      </li>
                      <li className="nav-item m-1">
                        <a
                          className="nav-link"
                          data-bs-toggle="tab"
                          role="tab"
                          aria-current="page"
                          href="#pending"
                          aria-selected="true"
                        >
                          Pending
                        </a>
                      </li>
                      <li className="nav-item m-1">
                        <a
                          className="nav-link"
                          data-bs-toggle="tab"
                          role="tab"
                          aria-current="page"
                          href="#in-progress"
                          aria-selected="true"
                        >
                          In Progress
                        </a>
                      </li>
                      <li className="nav-item m-1">
                        <a
                          className="nav-link"
                          data-bs-toggle="tab"
                          role="tab"
                          aria-current="page"
                          href="#completed"
                          aria-selected="true"
                        >
                          Completed
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div>
                    <div className="dropdown">
                      <button
                        className="btn btn-icon btn-sm btn-light btn-wave waves-light waves-effect"
                        type="button"
                        data-bs-toggle="dropdown"
                        aria-expanded="false"
                      >
                        <i className="ti ti-dots-vertical"></i>
                      </button>
                      <ul className="dropdown-menu">
                        <li>
                          <a
                            className="dropdown-item"
                            href="javascript:void(0);"
                          >
                            Select All
                          </a>
                        </li>
                        <li>
                          <a
                            className="dropdown-item"
                            href="javascript:void(0);"
                          >
                            Share All
                          </a>
                        </li>
                        <li>
                          <a
                            className="dropdown-item"
                            href="javascript:void(0);"
                          >
                            Delete All
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="tab-content task-tabs-container">
            <div
              className="tab-pane show active p-0"
              id="all-tasks"
              role="tabpanel"
            >
              <div className="row" id="tasks-container">
                <div className="col-xl-4 task-card">
                  <div className="card custom-card task-pending-card">
                    <div className="card-body">
                      <div className="d-flex justify-content-between flex-wrap gap-2">
                        <div>
                          <p className="fw-semibold mb-3 d-flex align-items-center">
                            <a href="javascript:void(0);">
                              <i className="ri-star-s-fill fs-16 op-5 me-1 text-muted"></i>
                            </a>
                            New Project Blueprint
                          </p>
                          <p className="mb-3">
                            Assigned On :{" "}
                            <span className="fs-12 mb-1 text-muted">
                              13,Nov 2022
                            </span>
                          </p>
                          <p className="mb-3">
                            Target Date :{" "}
                            <span className="fs-12 mb-1 text-muted">
                              20,Nov 2022
                            </span>
                          </p>
                          <p className="mb-0">
                            Assigned To :
                            <span className="avatar-list-stacked ms-1">
                              <span className="avatar avatar-sm avatar-rounded">
                                <img
                                  src="../assets/images/faces/2.jpg"
                                  alt="img"
                                />
                              </span>
                              <span className="avatar avatar-sm avatar-rounded">
                                <img
                                  src="../assets/images/faces/8.jpg"
                                  alt="img"
                                />
                              </span>
                              <span className="avatar avatar-sm avatar-rounded">
                                <img
                                  src="../assets/images/faces/2.jpg"
                                  alt="img"
                                />
                              </span>
                              <span className="avatar avatar-sm avatar-rounded">
                                <img
                                  src="../assets/images/faces/10.jpg"
                                  alt="img"
                                />
                              </span>
                            </span>
                          </p>
                        </div>
                        <div>
                          <div className="btn-list">
                            <button className="btn btn-sm btn-icon btn-wave btn-primary-light">
                              <i className="ri-edit-line"></i>
                            </button>
                            <button className="btn btn-sm btn-icon btn-wave btn-danger-light me-0">
                              <i className="ri-delete-bin-line"></i>
                            </button>
                          </div>
                          <span className="badge bg-warning-transparent d-block">
                            High
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card custom-card task-inprogress-card">
                    <div className="card-body">
                      <div className="d-flex justify-content-between flex-wrap gap-2">
                        <div>
                          <p className="fw-semibold mb-3 d-flex align-items-center">
                            <a href="javascript:void(0);">
                              <i className="ri-star-s-fill fs-16 me-1 text-warning"></i>
                            </a>
                            Designing New Authentication Pages
                          </p>
                          <p className="mb-3">
                            Assigned On :{" "}
                            <span className="fs-12 mb-1 text-muted">
                              26,Nov 2022
                            </span>
                          </p>
                          <p className="mb-3">
                            Target Date :{" "}
                            <span className="fs-12 mb-1 text-muted">
                              12,Dec 2022
                            </span>
                          </p>
                          <p className="mb-0">
                            Assigned To :
                            <span className="avatar-list-stacked ms-1">
                              <span className="avatar avatar-sm avatar-rounded">
                                <img
                                  src="../assets/images/faces/6.jpg"
                                  alt="img"
                                />
                              </span>
                              <span className="avatar avatar-sm avatar-rounded">
                                <img
                                  src="../assets/images/faces/15.jpg"
                                  alt="img"
                                />
                              </span>
                            </span>
                          </p>
                        </div>
                        <div>
                          <div className="btn-list">
                            <button className="btn btn-sm btn-icon btn-wave btn-primary-light">
                              <i className="ri-edit-line"></i>
                            </button>
                            <button className="btn btn-sm btn-icon btn-wave btn-danger-light me-0">
                              <i className="ri-delete-bin-line"></i>
                            </button>
                          </div>
                          <span className="badge bg-success-transparent d-block">
                            Low
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card custom-card task-completed-card">
                    <div className="card-body">
                      <div className="d-flex justify-content-between flex-wrap gap-2">
                        <div>
                          <p className="fw-semibold mb-3 d-flex align-items-center">
                            <a href="javascript:void(0);">
                              <i className="ri-star-s-fill fs-16 op-5 me-1 text-muted"></i>
                            </a>
                            Developing New Events in Plugin
                          </p>
                          <p className="mb-3">
                            Assigned On :{" "}
                            <span className="fs-12 mb-1 text-muted">
                              5,Dec 2022
                            </span>
                          </p>
                          <p className="mb-3">
                            Target Date :{" "}
                            <span className="fs-12 mb-1 text-muted">
                              10,Dec 2022
                            </span>
                          </p>
                          <p className="mb-0">
                            Assigned To :
                            <span className="avatar-list-stacked ms-1">
                              <span className="avatar avatar-sm avatar-rounded">
                                <img
                                  src="../assets/images/faces/5.jpg"
                                  alt="img"
                                />
                              </span>
                              <span className="avatar avatar-sm avatar-rounded">
                                <img
                                  src="../assets/images/faces/8.jpg"
                                  alt="img"
                                />
                              </span>
                              <span className="avatar avatar-sm avatar-rounded">
                                <img
                                  src="../assets/images/faces/11.jpg"
                                  alt="img"
                                />
                              </span>
                            </span>
                          </p>
                        </div>
                        <div>
                          <div className="btn-list">
                            <button className="btn btn-sm btn-icon btn-wave btn-primary-light">
                              <i className="ri-edit-line"></i>
                            </button>
                            <button className="btn btn-sm btn-icon btn-wave btn-danger-light me-0">
                              <i className="ri-delete-bin-line"></i>
                            </button>
                          </div>
                          <span className="badge bg-primary-transparent d-block">
                            Medium
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <ul className="pagination justify-content-end">
          <li className="page-item disabled">
            <a className="page-link">Previous</a>
          </li>
          <li className="page-item">
            <a className="page-link" href="javascript:void(0);">
              1
            </a>
          </li>
          <li className="page-item">
            <a className="page-link" href="javascript:void(0);">
              2
            </a>
          </li>
          <li className="page-item">
            <a className="page-link" href="javascript:void(0);">
              3
            </a>
          </li>
          <li className="page-item">
            <a className="page-link" href="javascript:void(0);">
              Next
            </a>
          </li>
        </ul>
      </div>
    </>
  );
};

export default NavbarSetting;
